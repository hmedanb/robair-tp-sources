#include <datmo.h>

datmo::datmo()
{

    sub_scan = n.subscribe("scan", 1, &datmo::scanCallback, this);
    sub_robot_moving = n.subscribe("robot_moving", 1, &datmo::robot_movingCallback, this);

    // communication with action
    pub_datmo = n.advertise<geometry_msgs::Point>("goal_to_reach", 1); // Preparing a topic to publish the goal to reach.

    pub_datmo_marker = n.advertise<visualization_msgs::Marker>("datmo_marker", 1); // Preparing a topic to publish our results. This will be used by the visualization tool rviz

    new_laser = false;
    new_robot = false;
    init_laser = false;
    init_robot = false;

    previous_robot_moving = true;

    ros::Rate r(10);

    while (ros::ok())
    {
        ros::spinOnce();
        update();
        r.sleep();
    }

}

datmo::datmo(char *goal_name)
{

    sub_scan = n.subscribe("scan", 1, &datmo::scanCallback, this);
    sub_robot_moving = n.subscribe("robot_moving", 1, &datmo::robot_movingCallback, this);

    // communication with action
    pub_datmo = n.advertise<geometry_msgs::Point>(goal_name, 1); // Preparing a topic to publish the goal to reach.

    pub_datmo_marker = n.advertise<visualization_msgs::Marker>("datmo_marker", 1); // Preparing a topic to publish our results. This will be used by the visualization tool rviz

    new_laser = false;
    new_robot = false;
    init_laser = false;
    init_robot = false;

    previous_robot_moving = true;

    ros::Rate r(10);

    while (ros::ok())
    {
        ros::spinOnce();
        update();
        r.sleep();
    }

}

// DETECT MOTION FOR BOTH LASER
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::store_background()
{
    // store all the hits of the laser in the background table

    ROS_INFO("storing background");

    for (int loop_hit = 0; loop_hit < nb_beams; loop_hit++)
        background[loop_hit] = r[loop_hit];

    ROS_INFO("background stored");

} // store_background

void datmo::reset_motion()
{
    // TO COMPLETE
    // for each hit, we reset the dynamic table

    ROS_INFO("reset motion");

    ROS_INFO("reset_motion done");

} // reset_motion

void datmo::detect_motion()
{
    // TO COMPLETE
    // for each hit, compare the current range with the background to detect motion

    ROS_INFO("detecting motion");

    ROS_INFO("motion detected");

} // detect_motion

// CLUSTERING FOR LASER DATA
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
// Distance between two points
float datmo::distancePoints(geometry_msgs::Point pa, geometry_msgs::Point pb)
{

    return sqrt(pow((pa.x - pb.x), 2.0) + pow((pa.y - pb.y), 2.0));
}

void datmo::perform_clustering()
{
    
    ROS_INFO("performing clustering");

    perform_basic_clustering();
    perform_advanced_clustering();

    ROS_INFO("clustering performed");

} // perform_clustering

void datmo::perform_basic_clustering()
{
    // TO COMPLETE
    // we perform the clustering as described in the lecture on perception
    // the data related to each cluster are stored in cluster_start, cluster_end and nb_cluster: see datmo.h for more details

    ROS_INFO("performing basic clustering");

    nb_clusters = 0;

    for (int loop_hit = 1; loop_hit < nb_beams; loop_hit++)
    {
        // TO COMPLETE
        /*     if EUCLIDIAN DISTANCE between (the previous hit and the current one) is higher than "cluster_threshold"
                {//the current hit doesnt belong to the same cluster*/
    }

     //Dont forget to update the different information for the last cluster
    //...

    ROS_INFO("basic clustering performed");

} // perform_basic_clustering

void datmo::perform_advanced_clustering()
{
    // TO COMPLETE
    /* for each cluster, we update:
        - cluster_size to store the size of the cluster ie, the euclidian distance between the first hit of the cluster and the last one
        - cluster_middle to store the middle of the cluster
        - cluster_dynamic to store the percentage of hits of the current cluster that are dynamic*/

    ROS_INFO("perform advanced clustering");

    for (int loop_cluster = 0; loop_cluster < nb_clusters; loop_cluster++)
    {
        int start = cluster_start[loop_cluster];
        int end = cluster_end[loop_cluster];
        ROS_INFO("TO COMPLETE");        

    }

    ROS_INFO("advanced clustering performed");

} // perform_advanced_clustering

int datmo::compute_nb_dynamic(int start, int end)
{
    //TO COMPLETE
    // return the number of points that are dynamic between start and end

    int nb_dynamic = 0;
 
    return (nb_dynamic);

} // compute_nb_dynamic

// DETECTION OF PERSONS
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::detect_legs()
{
    // TO COMPLETE
    // a leg is a cluster:
        // - with a size higher than "leg_size_min";
        // - with a size lower than "leg_size_max;
        // if more than "dynamic_threshold"% of its hits are dynamic the leg is considered to be dynamic
    // we update the array leg_cluster, leg_detected and leg_dynamic

    ROS_INFO("detecting legs");
    nb_legs_detected = 0;
    for (int loop=0; loop<nb_clusters; loop++)//loop over all the clusters
    {
        ROS_INFO("TO COMPLETE");
    }
   
    ROS_INFO("legs detected");

} // detect_legs

void datmo::detect_persons()
{

    //TO COMPLETE
    // a person has two legs located at less than "legs_distance_max" one from the other
    // a moving person (ie, person_dynamic array) has 2 legs that are dynamic
    // we update the person_detected table to store the middle of the person
    // we update the person_dynamic table to know if the person is moving or not      

    ROS_INFO("detecting persons");
    nb_persons_detected = 0;

    for (int loop_leg_right = 0; loop_leg_right < nb_legs_detected; loop_leg_right++)
        for (int loop_leg_left = loop_leg_right + 1; loop_leg_left < nb_legs_detected; loop_leg_left++)
        {
            ROS_INFO("TO COMPLETE");
        }
    
    ROS_INFO("persons detected");

} // detect_persons

void datmo::detect_a_moving_person()
{

    // TO COMPLETE
    // we store the moving_person_detected in preson_tracked
    // we update is_person_tracked
    // do not forget to publish person_tracked
    ROS_INFO("detecting a moving person");

    for (int loop_persons = 0; loop_persons < nb_persons_detected; loop_persons++)
        if (person_dynamic[loop_persons])
        {
            ROS_INFO("TO COMPLETE");
        }

    ROS_INFO("a moving person detected");

} // detect_a_moving_person

// TRACKING OF A PERSON
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::track_a_person()
{

    ROS_INFO("tracking a person");

    associated = false;
    float distance_min = uncertainty_max;
    int index_min;

    // association between the tracked person and the possible detection
    for (int loop_persons = 0; loop_persons < nb_persons_detected; loop_persons++)
    {
        float current_dist = distancePoints(person_tracked, person_detected[loop_persons]);
        if (current_dist < distance_min)
        {
            index_min = loop_persons;
            distance_min = current_dist;
            associated = true;
        }
    }

    if (associated)
    {
        person_tracked = person_detected[index_min];
        if (frequency < frequency_max)
            frequency++;
        uncertainty = uncertainty_min;
        pub_datmo.publish(person_tracked);
    }
    else
    {
        frequency--;
        if (uncertainty < uncertainty_max)
            uncertainty += uncertainty_inc;

        is_person_tracked = frequency > 0;
    }

    ROS_INFO("tracking of a person done");
}

// CALLBACKS
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::scanCallback(const sensor_msgs::LaserScan::ConstPtr &scan)
{

    new_laser = true;
    init_laser = true;

    // store the important data related to laserscanner
    range_min = scan->range_min;
    range_max = scan->range_max;
    angle_min = scan->angle_min;
    angle_max = scan->angle_max;
    angle_inc = scan->angle_increment;
    nb_beams = ((-1 * angle_min) + angle_max) / angle_inc;

    // store the range and the coordinates in cartesian framework of each hit
    float beam_angle = angle_min;
    for (int loop = 0; loop < nb_beams; loop++, beam_angle += angle_inc)
    {
        if ((scan->ranges[loop] < range_max) && (scan->ranges[loop] > range_min))
            r[loop] = scan->ranges[loop];
        else
            r[loop] = range_max;
        theta[loop] = beam_angle;

        // transform the scan in cartesian framewrok
        current_scan[loop].x = r[loop] * cos(beam_angle);
        current_scan[loop].y = r[loop] * sin(beam_angle);
        current_scan[loop].z = 0.0;
        // ROS_INFO("laser[%i]: (%f, %f) -> (%f, %f)", loop, range[loop], beam_angle*180/M_PI, current_scan[loop].x, current_scan[loop].y);
    }

} // scanCallback

void datmo::robot_movingCallback(const std_msgs::Bool::ConstPtr &state)
{

    new_robot = true;
    init_robot = true;
    current_robot_moving = state->data;

} // robot_movingCallback

// GRAPHICAL DISPLAY
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::reset_display()
{

    nb_pts = 0;
}

void datmo::display_motion()
{

    ROS_INFO("\n");
    ROS_INFO("display motion");
    for (int loop_hit = 0; loop_hit < nb_beams; loop_hit++)
        if (dynamic[loop_hit])
        {
            display[nb_pts] = current_scan[loop_hit];

            colors[nb_pts].r = 1;
            colors[nb_pts].g = 1;
            colors[nb_pts].b = 0;
            colors[nb_pts].a = 1.0;

            nb_pts++;
        }

    ROS_INFO("%i points are dynamic", nb_pts);
    ROS_INFO("motion displayed");

} // display_motion

void datmo::display_clustering()
{

    ROS_INFO("\n");
    ROS_INFO("display clusters");
    ROS_INFO("%d clusters have been detected.", nb_clusters);

    for (int loop_cluster = 0; loop_cluster < nb_clusters; loop_cluster++)
    {

        int start = cluster_start[loop_cluster];
        int end = cluster_end[loop_cluster];

        ROS_INFO("cluster[%i] = middle(%f, %f): current_scan[%i](%f, %f) -> current_scan[%i](%f, %f), size: %f, nb_dynamic: %i, percentage_dynamic: %i",
                 loop_cluster,
                 cluster_middle[loop_cluster].x,
                 cluster_middle[loop_cluster].y,
                 start,
                 current_scan[start].x,
                 current_scan[start].y,
                 end,
                 current_scan[end].x,
                 current_scan[end].y,
                 cluster_size[loop_cluster],
                 compute_nb_dynamic(start, end),
                 cluster_dynamic[loop_cluster]);

        // graphical display of the start of the current cluster in green
        display[nb_pts] = current_scan[start];
        colors[nb_pts].r = 0;
        colors[nb_pts].g = 1;
        colors[nb_pts].b = 0;
        colors[nb_pts].a = 1.0;
        nb_pts++;

        // graphical display of the end of the current cluster in red
        display[nb_pts] = current_scan[end];
        colors[nb_pts].r = 1;
        colors[nb_pts].g = 0;
        colors[nb_pts].b = 0;
        colors[nb_pts].a = 1.0;
        nb_pts++;

        // graphical display of the middle of the current cluster in white if static and yellow if dynamic
        display[nb_pts] = cluster_middle[loop_cluster];
        colors[nb_pts].r = 1;
        colors[nb_pts].g = 1;
        if (cluster_dynamic[loop_cluster] >= dynamic_threshold)
            colors[nb_pts].b = 0;
        else
            colors[nb_pts].b = 1;
        colors[nb_pts].a = 1.0;
        nb_pts++;
    }

    ROS_INFO("clusters displayed");

} // display_clusters

void datmo::display_legs()
{

    ROS_INFO("\n");
    ROS_INFO("display legs");
    ROS_INFO("%d legs have been detected.\n", nb_legs_detected);

    for (int loop_leg = 0; loop_leg < nb_legs_detected; loop_leg++)
    {
        int cluster = leg_cluster[loop_leg];

        if (leg_dynamic[loop_leg])
        {
            ROS_INFO("moving leg found: %i -> cluster = %i, (%f, %f), size: %f, dynamic: %i",
                     loop_leg,
                     cluster,
                     leg_detected[loop_leg].x,
                     leg_detected[loop_leg].y,
                     cluster_size[cluster],
                     cluster_dynamic[cluster]);
        }
        else
        {
            ROS_INFO("static leg found: %i -> cluster = %i, (%f, %f), size: %f, dynamic: %i",
                     loop_leg,
                     cluster,
                     leg_detected[loop_leg].x,
                     leg_detected[loop_leg].y,
                     cluster_size[cluster],
                     cluster_dynamic[cluster]);
        }

        for (int loop_hit = 0; loop_hit < nb_beams; loop_hit++)
            if (loop_hit >= cluster_start[cluster] && loop_hit <= cluster_end[cluster])
            {

                // moving legs are yellow
                display[nb_pts] = current_scan[loop_hit];

                if (leg_dynamic[loop_leg])
                {
                    colors[nb_pts].r = 1;
                    colors[nb_pts].g = 1;
                    colors[nb_pts].b = 0;
                    colors[nb_pts].a = 1.0;
                }
                else
                {
                    colors[nb_pts].r = 1;
                    colors[nb_pts].g = 1;
                    colors[nb_pts].b = 1;
                    colors[nb_pts].a = 1.0;
                }

                nb_pts++;
            }
    }

    ROS_INFO("legs displayed");

} // display_legs

void datmo::display_persons()
{

    ROS_INFO("\n");
    ROS_INFO("displaying persons");
    ROS_INFO("%d persons have been detected", nb_persons_detected);

    for (int loop_persons = 0; loop_persons < nb_persons_detected; loop_persons++)
    {
        int left = leg_left[loop_persons];
        int right = leg_right[loop_persons];

        display[nb_pts] = person_detected[loop_persons];

        if (person_dynamic[loop_persons])
        {
            ROS_INFO("moving person detected[%i](%f, %f): leg[%i](%f, %f) + leg[%i](%f, %f)",
                     loop_persons,
                     person_detected[loop_persons].x,
                     person_detected[loop_persons].y,
                     right,
                     leg_detected[right].x,
                     leg_detected[right].y,
                     left,
                     leg_detected[left].x,
                     leg_detected[left].y);

            colors[nb_pts].r = 0;
            colors[nb_pts].g = 1;
            colors[nb_pts].b = 0;
            colors[nb_pts].a = 1.0;
        }
        else
        {
            ROS_INFO("static person detected[%i](%f, %f): leg[%i](%f, %f) + leg[%i](%f, %f)",
                     loop_persons,
                     person_detected[loop_persons].x,
                     person_detected[loop_persons].y,
                     right,
                     leg_detected[right].x,
                     leg_detected[right].y,
                     left,
                     leg_detected[left].x,
                     leg_detected[left].y);

            colors[nb_pts].r = 1;
            colors[nb_pts].g = 0;
            colors[nb_pts].b = 0;
            colors[nb_pts].a = 1.0;
        }
        nb_pts++;
    }

    ROS_INFO("persons displayed");

} // display_persons

void datmo::display_a_tracked_person()
{

    ROS_INFO("\n");
    ROS_INFO("displaying the tracked person");

    if (is_person_tracked)
        if (associated)
        {
            ROS_INFO("the tracked person has been detected at: (%f, %f) with frequency = %i and uncertainty = %f",
                     person_tracked.x,
                     person_tracked.y,
                     frequency,
                     uncertainty);

            display[nb_pts] = person_tracked;

            colors[nb_pts].r = 0;
            colors[nb_pts].g = 1;
            colors[nb_pts].b = 0;
            colors[nb_pts].a = 1.0;

            nb_pts++;
        }
        else if (!associated)
        {
            ROS_INFO("the tracked person has not been detected and is still at: (%f, %f) with frequency = %i and uncertainty = %f",
                     person_tracked.x,
                     person_tracked.y,
                     frequency,
                     uncertainty);

            display[nb_pts] = person_tracked;

            colors[nb_pts].r = 1;
            colors[nb_pts].g = 0;
            colors[nb_pts].b = 0;
            colors[nb_pts].a = 1.0;

            nb_pts++;
        }
        else
            ROS_WARN("the tracked person has been lost");

    ROS_INFO("the tracked person displayed");
}

// Draw the field of view and other references
void datmo::populateMarkerReference()
{

    visualization_msgs::Marker references;

    references.header.frame_id = "laser";
    references.header.stamp = ros::Time::now();
    references.ns = "datmo_marker";
    references.id = 1;
    references.type = visualization_msgs::Marker::LINE_STRIP;
    references.action = visualization_msgs::Marker::ADD;
    references.pose.orientation.w = 1;

    references.scale.x = 0.02;

    references.color.r = 1.0f;
    references.color.g = 1.0f;
    references.color.b = 1.0f;
    references.color.a = 1.0;
    geometry_msgs::Point v;

    v.x = 0.02 * cos(-2.356194);
    v.y = 0.02 * sin(-2.356194);
    v.z = 0.0;
    references.points.push_back(v);

    v.x = 5.6 * cos(-2.356194);
    v.y = 5.6 * sin(-2.356194);
    v.z = 0.0;
    references.points.push_back(v);

    float beam_angle = -2.356194 + 0.006136;
    // first and last beam are already included
    for (int i = 0; i < 723; i++, beam_angle += 0.006136)
    {
        v.x = 5.6 * cos(beam_angle);
        v.y = 5.6 * sin(beam_angle);
        v.z = 0.0;
        references.points.push_back(v);
    }

    v.x = 5.6 * cos(2.092350);
    v.y = 5.6 * sin(2.092350);
    v.z = 0.0;
    references.points.push_back(v);

    v.x = 0.02 * cos(2.092350);
    v.y = 0.02 * sin(2.092350);
    v.z = 0.0;
    references.points.push_back(v);

    pub_datmo_marker.publish(references);
}

void datmo::display_graphical_markers()
{

    visualization_msgs::Marker marker;

    marker.header.frame_id = "laser";
    marker.header.stamp = ros::Time::now();
    marker.ns = "datmo_marker";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::POINTS;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.orientation.w = 1;

    marker.scale.x = 0.05;
    marker.scale.y = 0.05;

    marker.color.a = 1.0;

    // ROS_INFO("%i points to display", nb_pts);
    for (int loop = 0; loop < nb_pts; loop++)
    {
        geometry_msgs::Point p;
        std_msgs::ColorRGBA c;

        p.x = display[loop].x;
        p.y = display[loop].y;
        p.z = display[loop].z;

        c.r = colors[loop].r;
        c.g = colors[loop].g;
        c.b = colors[loop].b;
        c.a = colors[loop].a;

        // ROS_INFO("(%f, %f, %f) with rgba (%f, %f, %f, %f)", p.x, p.y, p.z, c.r, c.g, c.b, c.a);
        marker.points.push_back(p);
        marker.colors.push_back(c);
    }

    pub_datmo_marker.publish(marker);
    populateMarkerReference();
}